# hk - mouse hot keys for windows (experiment)

Mouse hot keys for windows. Run hk.exe

The ````ALT key + <number pad key>```` moves the mouse pointer 1/2 a step in the direction of the key on the kaypad. Requires a number pad.

  ```
  DIVIDE - center mouse cursor
  Numpad 0 - left click
  Numpad 1 - right click
  ```

Built using mingw64 from msys2 installation.

Make sure you use a mingw shell to build and not the msys shell

run:
```
make
```
