CFLAGS=-g -DWINDOWS -DWINVER=0x0601 -D_UNICODE
LDFLAGS=-Wl,-subsystem,console
LD=cc


.SUFFIXES: .exe .o 
all: hk.exe

hk.exe: LIBS:=-luser32

.c.o:
	$(CC) $(CFLAGS) -c $^ -o $@

.o.exe:  
	$(LD) $(LDFLAGS) $^ -o $@ $(LIBS)

