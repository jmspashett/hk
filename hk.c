#include <windows.h>
#include <winuser.h>
#include <stdio.h>
#include <tchar.h>
#include <assert.h>

//const int modKey = MOD_ALT | MOD_NOREPEAT;
const int modKey = MOD_ALT;

enum MouseButton { MouseButton_none, MouseButton_left, MouseButton_right};
void sendMouseClick(int x, int y, int mouseButton)
{
    INPUT input;
    input.type=INPUT_MOUSE;
    input.mi.dx=x;
    input.mi.dy=y;
    input.mi.dwFlags=0;//MOUSEEVENTF_ABSOLUTE|MOUSEEVENTF_MOVE|
    if (mouseButton == MouseButton_left) {
        input.mi.dwFlags |= MOUSEEVENTF_LEFTDOWN;
        input.mi.dwFlags |= MOUSEEVENTF_LEFTUP;
    } else if (mouseButton == MouseButton_right) {
        input.mi.dwFlags |= MOUSEEVENTF_RIGHTDOWN;
        input.mi.dwFlags |= MOUSEEVENTF_RIGHTUP;
    }

    input.mi.mouseData=0;
    input.mi.dwExtraInfo=0;
    input.mi.time=0;
    SendInput(1,&input,sizeof(INPUT));
}

POINT maximumTravel(POINT mousePos, RECT desktopRect)
{
    POINT travel;

    travel.x = max(mousePos.x, desktopRect.right - mousePos.x);
    travel.y = max(mousePos.y, desktopRect.bottom - mousePos.y);
    return travel;
}

void halfMovement(POINT * split, int x, int y)
{
    const int minimumSpeed = 10;
    if (x) 
        split->x = max(split->x / 2, minimumSpeed);
    if (y) 
        split->y = max(split->y / 2, minimumSpeed);
}

void moveLeft(POINT * pos, POINT * split)
{
    pos->x -= split->x;
    halfMovement(split, 1, 0);
    split->x = max(1, split->x);
}

void moveRight(POINT * const pos, POINT * const split)
{
    pos->x += split->x;
    halfMovement(split, 1, 0);
    split->x = max(1, split->x);
}

void moveUp(POINT * const pos, POINT * const split)
{
    pos->y -= split->y;
    halfMovement(split, 0, 1);
    split->y = max(1, split->y);
}

void moveDown(POINT * const pos, POINT * const split)
{
    pos->y += split->y;
    halfMovement(split, 0, 1);
    split->y = max(1, split->y);
}

void travelCentre(POINT * const split, POINT const * const pos, 
                  RECT const * desktopRect)
{
    POINT travel = maximumTravel(*pos, *desktopRect);
    split->x = travel.x / 2; 
    split->y = travel.y / 2;
}

void moveCentre(POINT * const pos, POINT * const split, 
                POINT const * const desktopDim, 
                RECT const * const desktopRect)
{
    pos->x = desktopDim->x / 2;
    pos->y = desktopDim->y / 2;
    travelCentre(split, pos, desktopRect);
}

int main(int argc, wchar_t *args[])
{
    fclose(stderr);
    int err=0;

    err |= !RegisterHotKey( NULL, 1, modKey, VK_DIVIDE);  
    assert(("VK_DIVIDE", !err));

    err |= !RegisterHotKey( NULL, 1, modKey, VK_NUMPAD7);  
    assert(("VK_NUMPAD7", !err));

    err |= !RegisterHotKey( NULL, 1, modKey, VK_NUMPAD8);  
    assert(("VK_NUMPAD8", !err));

    err |= !RegisterHotKey( NULL, 1, modKey, VK_NUMPAD9);  
    assert(("VK_NUMPAD9", !err));

    err |= !RegisterHotKey( NULL, 1, modKey, VK_NUMPAD4);  
    assert(("VK_NUMPAD4", !err));

    err |= !RegisterHotKey( NULL, 1, modKey, VK_NUMPAD5);  
    assert(("VK_NUMPAD5", !err));

    err |= !RegisterHotKey( NULL, 1, modKey, VK_NUMPAD6);  
    assert(("VK_NUMPAD6", !err));

    err |= !RegisterHotKey( NULL, 1, modKey, VK_NUMPAD1);  
    assert(("VK_NUMPAD1", !err));

    err |= !RegisterHotKey( NULL, 1, modKey, VK_NUMPAD2);  
    assert(("VK_NUMPAD2", !err));

    err |= !RegisterHotKey( NULL, 1, modKey, VK_NUMPAD3);  
    assert(("VK_NUMPAD3", !err));

    err |= !RegisterHotKey( NULL, 1, modKey, VK_NUMPAD0);  
    assert(("VK_NUMPAD0", !err));

    err |= !RegisterHotKey( NULL, 1, modKey, VK_DECIMAL);  
    assert(("VK_DECIMAL", !err));

    if (err) {
        fwprintf(stderr, L"Failed to set hotkey\n");
        abort();
    }
    fflush(stderr); fflush(stdout);
#if 1 
    fwprintf(stderr, L"Starting message loop\n");
    MSG msg = {0};


    RECT desktopRect;
    const HWND desktopH = GetDesktopWindow();
    GetWindowRect(desktopH, &desktopRect);
    const POINT desktopDim = { 
            desktopRect.right - desktopRect.left,
            desktopRect.bottom - desktopRect.top
        };

    POINT split = {10, 10};


    while (GetMessage(&msg, NULL, 0, 0) != 0)
    {
        if (msg.message == WM_HOTKEY)
        {
            POINT pos;
            GetCursorPos(&pos);
            fprintf(stderr, "pos (x, y) = (%d, %d)\n", pos.x, pos.y);
            fprintf(stderr, "split = %d,%d\n", split.x, split.y);
            switch (HIWORD(msg.lParam)) {
                case VK_NUMPAD5:
                    moveCentre(&pos, &split, &desktopDim, &desktopRect);
                    break;
                case VK_DIVIDE: 
                    travelCentre(&split, &pos, &desktopRect); 
                    break;
                case VK_NUMPAD8:
                    moveUp(&pos, &split);
                    break;
                case VK_NUMPAD6:
                    moveRight(&pos, &split);
                    break;
                case VK_NUMPAD2:
                    moveDown(&pos, &split);
                    break;
                case VK_NUMPAD3:
                    moveDown(&pos, &split);
                    moveRight(&pos, &split);
                    break;
                case VK_NUMPAD9:
                    moveUp(&pos, &split);
                    moveRight(&pos, &split);
                    break;
                case VK_NUMPAD4:
                    moveLeft(&pos, &split);
                    break;
                case VK_NUMPAD1:
                    moveDown(&pos, &split);
                    moveLeft(&pos, &split);
                    break;
                case VK_NUMPAD7:
                    moveUp(&pos, &split);
                    moveLeft(&pos, &split);
                    break;
				case VK_NUMPAD0:
					sendMouseClick(pos.x, pos.y, MouseButton_left);
					break;
				case VK_DECIMAL:
					sendMouseClick(pos.x, pos.y, MouseButton_right);
					break;
            }
            fprintf(stderr, "Moving cursor to (%d, %d)\n", pos.x, pos.y);
            SetCursorPos(pos.x, pos.y);
            fprintf(stderr, "WM_HOTKEY %d\n", HIWORD(msg.lParam));            
            fflush(stderr); fflush(stdout);
        }
    } 
#endif 
    return 0;
}

